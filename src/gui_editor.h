#ifndef GUI_CODE_EDITOR
#define GUI_CODE_EDITOR

#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>

#define DEFAULT_EDITOR_FONT (const gchar*) "Monospace 12"
#define HIGHLIGHT_TAG (const gchar*) "hl_tag"
#define COLOUR_BG_HL (const gchar*) "#00FFFF"

typedef struct gui_editor GUIEditor;

struct gui_editor
{
	GtkWidget *widget;
	const gchar* current_font;
	GtkSourceBuffer *buffer;
	GtkTextMark *mark;
	GtkTextTag *hltag;
	GtkTextIter iter;
};

GUIEditor *gui_editor_new();

void gui_editor_show(GUIEditor *self);

void gui_editor_destroy(GUIEditor *self);

void gui_editor_set_highlight(GUIEditor *self, guint line_no, gboolean set);

void gui_editor_clear_all_highlights(GUIEditor *self);

void gui_editor_goto_line(GUIEditor *self, gint ln);

#endif
