#ifndef ARCHI_H
#define ARCHI_H

#include <stdbool.h>

// contains the memory and registers for the simulator

#define MEM_SIZE (1 << 15)

extern int mem[];

int view_mem(int addr);

void write_mem(int address, int data);

int view_word(int addr);

void write_word(int address, int data);

int view_reg(char const *reg_name);

typedef struct reg Reg;

struct reg {
	const char *name;
	int no;
	int val;
};

extern Reg a;
extern Reg x;
extern Reg l;
extern Reg pc;
extern Reg sw;

#define N_REGS (1 << 4)

extern Reg *regs[];

void regs_init();

#define MAX_BUF 100

#define N_INPUT_PORTS 5

typedef struct port {
	int r_ptr;
	int w_ptr;
	char buf[MAX_BUF + 1];
} Port;

bool is_port_empty(const Port *port);
bool is_port_full(const Port *port);

extern Port ports[];

void ports_init();

void enter_str_to_port(int port_no, const char *str);

char *read_str_from_port(char *str, int port_no);

#endif
