#include "runner.h"
#include "archi.h"
#include "instructions.h"
#include <stdio.h>

const int HALT_OPCODE = 0xfd;

void decode_instruc(int* opcode, int* address, int instruction)
{
	// check if indexing present
    *address = instruction & 0x7FFF;
    if(instruction & 0x8000)
    {
        *address += x.val;
    }
    
    *opcode = instruction & 0xFF0000;
    *opcode = *opcode >> 16;
}

int exec_instruc(int opcode, int addr)
{
	if (opcode == HALT_OPCODE) {
		return EXIT_PROG;
	}
	instructions[opcode](addr);
	return 0;
}

int exec_next()
{
	int ins = view_word(pc.val);
	int opcode, addr;
	pc.val += 3;
	decode_instruc(&opcode, &addr, ins);
	return exec_instruc(opcode, addr);
}

void exec_prog()
{
	while (exec_next() != EXIT_PROG);
	// do {
	// 	int instruction =view_word(pc.val);
	// 	pc.val += 3;
	// 	// printf("%d %d\n", pc.val, a.val);
	// 	
	// 	decode_instruc(&opcode,&address, instruction);
    // } while (exec_instruc(opcode,address) != EXIT_PROG);
	// implement ExecuteInstruction using the decode.h and hash table of fn pointers EXIT_PROGRAM similar to halt
	
}
