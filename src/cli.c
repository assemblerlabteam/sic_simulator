#include "simulator.h"
#include "runner.h"
#include "loader.h"
#include "assembler.h"
#include "archi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define INPUT_LEN 128

void print_help()
{
	printf("assemble[a], load[l], run[r], exec_next[en]\n");
	printf("view_mem[vm], view_regs[vr], view_word[vw]\n");
	printf("reset[res], quit[q]\n");
	printf("help[h]\n");
}

int main()
{ 
	start();
	char option[INPUT_LEN + 1] = "";
	bool quit = false;
	do {
		printf(">>> ");
		fgets(option, INPUT_LEN, stdin);
		option[strcspn(option, "\n")] = '\0';
		if (!strcmp(option, "a")) {
			printf("Enter input file: ");
			char input_file_name[INPUT_LEN + 1];
			fgets(input_file_name, INPUT_LEN, stdin);
			input_file_name[strcspn(input_file_name, "\n")] = '\0';
			const char *output_file_path = "../output/obj.txt";
			assemble(input_file_name, output_file_path);
		} else if (!strcmp(option, "l")) {
			char* c="../output/obj.txt";
			load(c);
		   //	load_interface();
		} else if (!strcmp(option, "r")) {
			exec_prog();
		} else if (!strcmp(option, "en")) {
			if (exec_next() == EXIT_PROG) {
				printf("HALT\n");
			} else {
				printf("RAN\n");
			}
		} else if (!strcmp(option, "vm")) {
			printf("Enter address: ");
			char addr_str[INPUT_LEN + 1];
			fgets(addr_str, INPUT_LEN, stdin);
			int addr = strtol(addr_str, NULL, 0);
			printf("Memory[0x%04X] = 0x%02X\n", addr, view_mem(addr));
		} else if (!strcmp(option, "vr")) {
			printf("A: 0x%06X\n", view_reg("A"));
			printf("X: 0x%06X\n", view_reg("X"));
			printf("L: 0x%06X\n", view_reg("L"));
			printf("PC: 0x%06X\n", view_reg("PC"));
			printf("SW: 0x%06X\n", view_reg("SW"));
		} else if (!strcmp(option, "vw")) {
			printf("Enter address: ");
			char addr_str[INPUT_LEN + 1];
			fgets(addr_str, INPUT_LEN, stdin);
			int addr = strtol(addr_str, NULL, 0);
			printf("0x%06X\n", view_word(addr));
		} else if (!strcmp(option, "res")) {
			reset();
		} else if (!strcmp(option, "q")) {
			quit = true;
		} else if (!strcmp(option, "h")) {
			print_help();
		} else {
			printf("Invalid input. Enter h for help.\n");
		}
	} while (!quit);
	return 0;
}
