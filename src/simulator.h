#ifndef SIMULATOR_H
#define SIMULATOR_H

// has to be called when the simulator starts
void start();

// resets all registers value to zero
void reset();

#endif
