#include "helper.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *strrev(char *str)
{
    /* skip null */
    if (str == 0) {
		return str;
    }

    /* skip empty string */
    if (*str == 0) {
		return str;
    }

    /* get range */
    char *start = str;
    char *end = start + strlen(str) - 1; /* -1 for \0 */
    char temp;

    /* reverse */
    while (end > start) {
		/* swap */
		temp = *start;
		*start = *end;
		*end = temp;

		/* move */
		++start;
		--end;
    }
	return str;
}

char *strtoupper(char *str)
{
	for (char *c = str; *c; c++) {
		*c = toupper(*c);
	}
	return str;
}

char *strtok_single(char *str, char const *delims)
{
	static char *src = NULL;
	char  *p,  *ret = 0;

	if (str != NULL)
		src = str;

	if (src == NULL)
		return NULL;

	if ((p = strpbrk(src, delims)) != NULL) {
		*p  = 0;
		ret = src;
		src = ++p;
	} else if (*src) {
		ret = src;
		src = NULL;
	}

	return ret;
}

char *hex_to_dec(char *res, char *input)
{
	int num = strtol(input, NULL, 16);
	sprintf(res, "%d", num);
	return res;
}

char *dec_to_hex(char *res, char *input)
{
	int num = strtol(input, NULL, 10);
	sprintf(res, "%X", num);
	return res;
}
