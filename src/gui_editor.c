#include "gui_editor.h"
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>

GUIEditor *gui_editor_new()
{
	GUIEditor *self = g_malloc0(sizeof *self);
	self->buffer = gtk_source_buffer_new(NULL);
	self->widget = gtk_source_view_new_with_buffer(self->buffer);

	self->hltag = gtk_text_buffer_create_tag(GTK_TEXT_BUFFER(self->buffer),
												HIGHLIGHT_TAG,
												"background", COLOUR_BG_HL,
												NULL);

	return self;
}

void gui_editor_show(GUIEditor *self)
{
	gtk_widget_show(GTK_WIDGET(self->widget));
	gtk_source_view_set_show_line_numbers(GTK_SOURCE_VIEW(self->widget), TRUE);
	self->mark = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(self->buffer));
	gtk_source_view_set_show_line_marks(GTK_SOURCE_VIEW(self->widget), TRUE);
}

void gui_editor_destroy(GUIEditor *self)
{
	gtk_widget_destroy(self->widget);
	g_free(self);
}

void gui_editor_set_highlight(GUIEditor *self, guint line_no, gboolean set)
{
	g_assert(self);
	GtkTextIter line_start, line_end;

	/* get line bounds */
	gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER(self->buffer), &line_start,(line_no -1));
	line_end = line_start;
	gtk_text_iter_forward_to_line_end(&line_end);

	if(set) {
		gtk_text_buffer_apply_tag(GTK_TEXT_BUFFER(self->buffer), self->hltag,
				&line_start, &line_end);
	} else {
		gtk_text_buffer_remove_tag(GTK_TEXT_BUFFER(self->buffer), self->hltag,
				&line_start, &line_end);
	}
}

void gui_editor_clear_all_highlights(GUIEditor *self)
{
	GtkTextIter buffer_start, buffer_end;

	gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(self->buffer), &buffer_start, &buffer_end);
	gtk_text_buffer_remove_tag(GTK_TEXT_BUFFER(self->buffer), self->hltag, &buffer_start, &buffer_end);
}

void gui_editor_goto_line(GUIEditor *self, gint ln)
{
	gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER(self->buffer), &(self->iter),(ln -1));

	gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(self->buffer), &(self->iter));

	gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(self->buffer), self->mark, &(self->iter));

	gtk_text_view_scroll_mark_onscreen(GTK_TEXT_VIEW(self->widget), self->mark);
}
