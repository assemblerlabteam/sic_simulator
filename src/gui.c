#include "assembler.h"
#include "simulator.h"
#include "runner.h"
#include "loader.h"
#include "archi.h"
#include "gui_editor.h"
#include <gtk/gtk.h>
#include <glib.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <gtksourceview/gtksource.h>

#define MAX_LEN 1024
#define MEM_SIZE (1 << 15)

GtkBuilder *builder;
GtkWidget  *window;
GtkWidget *text_view_code;
GtkWidget *label_reg_A_val;
GtkWidget *label_reg_X_val;
GtkWidget *label_reg_L_val;
GtkWidget *label_reg_PC_val;
GtkWidget *label_reg_SW_val;
GtkWidget *entry_dec;
GtkWidget *entry_hex;
GtkWidget *entry_content;
GtkWidget *entry_io_entry;
GtkWidget *scrolledwindow_mem;
GtkWidget *spinbutton_address;
GtkWidget *spinbutton_io;
GtkWidget *treeview;
GtkWidget *entry_jump_addr;
GtkListStore *list_store;
GtkTextBuffer *text_buffer_errors;

GUIEditor *editor;

char numberEntered[MAX_LEN] = "";
char textEntered[MAX_LEN] = "";
char curr_open_file[MAX_LEN + 1] = "";

void on_imagemenuitem_new_activate()
{
	GtkTextBuffer *buffer =
		gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view_code));
	GtkTextIter start, end;
	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);
	gtk_text_buffer_delete(buffer, &start, &end);
	strcpy(curr_open_file, "");
}

gchar *get_a_file_dialog(const char *title, const char *main_action)
{
	GtkWidget *dialog;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint res;

	dialog = gtk_file_chooser_dialog_new (title,
										  GTK_WINDOW(window),
										  action,
										  "_Cancel",
										  GTK_RESPONSE_CANCEL,
										  main_action,
										  GTK_RESPONSE_ACCEPT,
										  NULL);

	gchar *filename = NULL;
	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT)
	{
		GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
		filename = gtk_file_chooser_get_filename (chooser);
	}

	gtk_widget_destroy (dialog);

	return filename;
}

void load_text_buffer_from_file(GtkTextBuffer *buffer,
		const gchar *filename)
{
	gchar *contents;
	gsize length;
	GError *error;
	g_file_get_contents(filename, &contents, &length, &error);
	GtkTextIter start, end;
	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);
	gtk_text_buffer_set_text(buffer, contents, length);

	g_free(contents);
}

void on_imagemenuitem_open_activate()
{
	gchar *filename = get_a_file_dialog("Open File", "_Open");
	if (filename == NULL) {
		return;
	}

	gtk_widget_set_sensitive(text_view_code, FALSE);
	GtkTextBuffer *buffer = 
		gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view_code));
	load_text_buffer_from_file(buffer, filename);
	gtk_widget_set_sensitive(text_view_code, TRUE);

	strcpy(curr_open_file, filename);


	g_free(filename);
}

void save_text_buffer_to_file(const char *filename)
{
	gtk_widget_set_sensitive (text_view_code, FALSE);
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text_view_code));
	GtkTextIter start, end;
	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);
	gchar *contents = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);       
	// gtk_text_buffer_set_modified (buffer, FALSE);
	gtk_widget_set_sensitive (text_view_code, TRUE);

	/* set the contents of the file to the text from the buffer */
	GError *err = NULL;
	gboolean result = g_file_set_contents(filename, contents, -1, &err);

	if (result == FALSE)
	{
		/* error saving file, show message to user */
		g_print("%s\n", err->message);
		g_error_free (err);
	}        

	g_free(contents); 
}

void on_imagemenuitem_save_as_activate()
{
	GtkWidget *dialog;
	GtkFileChooser *chooser;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
	gint res;

	dialog = gtk_file_chooser_dialog_new ("Save File",
										  GTK_WINDOW(window),
										  action,
										  "_Cancel",
										  GTK_RESPONSE_CANCEL,
										  "_Save",
										  GTK_RESPONSE_ACCEPT,
										  NULL);
	chooser = GTK_FILE_CHOOSER (dialog);

	gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

	if (!strcmp(curr_open_file, "")) {
		gtk_file_chooser_set_current_name (chooser,
										 "Untitled document");
	} else {
		gtk_file_chooser_set_filename (chooser,
									 curr_open_file);
	}

	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT) {
		char *filename;

		filename = gtk_file_chooser_get_filename (chooser);
		strcpy(curr_open_file, filename);
		save_text_buffer_to_file(filename);
		g_free (filename);
	}

	gtk_widget_destroy (dialog);
}

void on_imagemenuitem_save_activate()
{
	if (!strcmp(curr_open_file, "")) {
		on_imagemenuitem_save_as_activate();
	}
	save_text_buffer_to_file(curr_open_file);
}

void set_regs()
{
  char buffer[100];

  int val=view_reg("A");
  sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_reg_A_val),(const char*)&buffer);
  buffer[0]='\0';

  val=view_reg("X");
  sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_reg_X_val),(const char*)&buffer);
  buffer[0]='\0';

   val=view_reg("L");
   sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_reg_L_val),(const char*)&buffer);
  buffer[0]='\0';

   val=view_reg("PC");
  sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_reg_PC_val),(const char*)&buffer);
  buffer[0]='\0';

   val=view_reg("SW");
   sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_reg_SW_val),(const char*)&buffer);
  buffer[0]='\0';
}

void set_memory()
{
    int curr_addr = 0;
    GtkTreeIter iter;
	gboolean valid =
		gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list_store), &iter);
	while (valid) {
		char buffer[100];
		char buffer2[100];

		sprintf(buffer, "%04X", curr_addr);
		int content = view_mem(curr_addr);

		sprintf(buffer2, "%02X", content);
		gtk_list_store_set(list_store, &iter, 0, buffer,1,buffer2,-1); //(address+i) is appended in i-th row

		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(list_store), &iter);
		curr_addr++;
	}
}

void highlight_curr_line()
{
	FILE *debug_file = fopen("output/debug.txt", "r");
	int pc_value, line_no;
	while (fscanf(debug_file, "%x %d", &pc_value, &line_no) != EOF) {
		if (pc_value == pc.val) {
			gui_editor_set_highlight(editor, line_no, true);
			gui_editor_goto_line(editor, line_no);
			break;
		}
	}
	fclose(debug_file);
}

void on_button_assemble_clicked()
{
	// gui_editor_set_highlight(editor, 1, true);
	// printf("%s\n",curr_open_file);
	const char *obj_file_path = "output/obj.txt";

	char success_msg[MAX_LEN + 1] =
	  "Code assembled\nObject file stored at path: ";

	// char actual_path[MAX_LEN + 1] = "";
	strcat(success_msg, obj_file_path);
	strcat(success_msg, "\n");

	if (assemble(curr_open_file, obj_file_path)) {
		gtk_text_buffer_set_text(text_buffer_errors, success_msg, -1);
	} else {
		const char *error_file_path = "output/errors.txt";
		load_text_buffer_from_file(text_buffer_errors, error_file_path);
	}
}

void on_button_load_clicked()
{
	gchar *filename = get_a_file_dialog("Load Object File", "_Load");
	if (filename == NULL) {
		return;
	}

	load(filename);
	set_regs();
	set_memory();

	const char *success_msg = "Object file loaded.\n";

	gtk_text_buffer_set_text(text_buffer_errors, success_msg, -1);

	g_free(filename);
	highlight_curr_line();
}

void on_button_run_clicked()
{
	exec_prog();
	set_regs();
	set_memory();
	const char *success_msg = "Code ran successfully.\n"; 
	gtk_text_buffer_set_text(text_buffer_errors, success_msg, -1);
	gui_editor_clear_all_highlights(editor);
}

void on_button_next_clicked()
{
	int ret=exec_next();
	set_regs();
	set_memory();

	if(ret==EXIT_PROG) {
		gtk_text_buffer_set_text(text_buffer_errors,
				"Received halt instruction. Halted.", -1);
	} else {
		gtk_text_buffer_set_text(text_buffer_errors,
				"Executed next instruction.", -1);
	}

	gui_editor_clear_all_highlights(editor);
	highlight_curr_line();
}

void on_button_reset_clicked()
{
	reset();
	set_regs();
	// gtk_label_set_text(GTK_LABEL(label_status),"Welcome");
	gtk_text_buffer_set_text(text_buffer_errors, "", -1);
	gui_editor_clear_all_highlights(editor);
}

void on_button_to_hex_clicked()
{
   char result[100];

   const char* text=gtk_entry_get_text(GTK_ENTRY(entry_dec));
   strcpy(numberEntered,text);

   int num = strtol(numberEntered, NULL, 10);
   sprintf(result, "%X", num);
  
  gtk_entry_set_text(GTK_ENTRY(entry_hex),(const char*)&result);
  result[0]='\0';
}

void on_button_to_dec_clicked()
{
   char result[100];

   const char* text=gtk_entry_get_text(GTK_ENTRY(entry_hex));
   strcpy(numberEntered,text);

   int num = strtol(numberEntered, NULL, 16);
   sprintf(result, "%d", num);
  
  gtk_entry_set_text(GTK_ENTRY(entry_dec),(const char*)&result);
  result[0]='\0';
}

void on_mem_list_data_edited (GtkCellRendererText *renderer, gchar *path,
                         gchar *new_text, gpointer user_data)
{
	GtkTreeIter iter;
	gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL (list_store), &iter, path);

	gint value = strtol(new_text, NULL, 0);

	char buffer[MAX_LEN + 1] = "";
	sprintf(buffer, "%02X", value & 0xff);
	gtk_list_store_set (list_store, &iter, 1, buffer, -1);
	gchar *addr;
	gtk_tree_model_get (GTK_TREE_MODEL(list_store), &iter, 0, &addr, -1);
	mem[strtol(addr, NULL, 6)] = value;
}

void attach_memory_list()      //address from where next 10 addresses along with their contents are to be dispalyed
{
	list_store = gtk_list_store_new(2, G_TYPE_STRING,G_TYPE_STRING);//2 denotes number of columns ,G_TYPE_STRING denotes type of values which are displayed in the respective columns
	GtkTreeIter treeiter;                             //to identify the rows
	//GtkWidget *treeview;                              //for the display in the form of adjacent columns
	//GtkCellRenderer *cellrenderertext2;                //for the heading of each column
	GtkTreeViewColumn *treeviewcolumn;                //the column objects which treeview takes as arguments

	int i;
	char buffer[100];
	char buffer2[100];


	/*to display memory addresses with contents*/
	for(i=0;i<MEM_SIZE;i++) {
		sprintf(buffer,"%04X",i);
		int content=view_mem(i);

		sprintf(buffer2,"%02X",content);
		gtk_list_store_append(list_store, &treeiter);
		gtk_list_store_set(list_store, &treeiter, 0, buffer,1,buffer2,-1); //(address+i) is appended in i-th row
		buffer[0]='\0';
		buffer2[0]='\0';
	}

	treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(list_store));
	gtk_container_add(GTK_CONTAINER(scrolledwindow_mem), treeview);
	gtk_tree_view_set_grid_lines(GTK_TREE_VIEW(treeview),
			GTK_TREE_VIEW_GRID_LINES_BOTH);//for the matrix type of display

	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	treeviewcolumn = gtk_tree_view_column_new_with_attributes("Memory address",
			renderer, "text", 0, NULL);
	gtk_tree_view_insert_column(GTK_TREE_VIEW(treeview), treeviewcolumn, 0);            //this column contains the memory addresses

	renderer = gtk_cell_renderer_text_new();
	g_object_set((gpointer)renderer, "editable", TRUE, NULL);
	g_signal_connect((gpointer) renderer, "edited",
			G_CALLBACK(on_mem_list_data_edited), NULL);
	treeviewcolumn = gtk_tree_view_column_new_with_attributes("Contents",
			renderer, "text", 1, NULL);
	gtk_tree_view_insert_column(GTK_TREE_VIEW(treeview), treeviewcolumn, 1);
	//gtk_tree_view_set_grid_lines(GTK_TREE_VIEW(treeview), GTK_TREE_VIEW_GRID_LINES_BOTH);

	gtk_widget_show_all(scrolledwindow_mem);
}


  
void on_button_view_mem_clicked()
{

 // const char* text=gtk_entry_get_text(GTK_ENTRY(entry_content));
  //strcpy(textEntered,text);

  //int addr=strtol(textEntered,NULL,0);
  int addr =
	  gtk_spin_button_get_value_as_int((GtkSpinButton *)spinbutton_address);

  
  // set_memory();
  
  int content=view_mem(addr);

  char buffer[100] = "";
  sprintf(buffer,"%02X",content);
  gtk_entry_set_text(GTK_ENTRY(entry_content),(const char*)&buffer);
  buffer[0]='\0';

}

void on_button_view_word_clicked()
{

  //const char* text=gtk_entry_get_text(GTK_ENTRY(entry_content));
  //strcpy(textEntered,text);

   //int addr=strtol(textEntered,NULL,0);
  int addr=gtk_spin_button_get_value_as_int((GtkSpinButton *)spinbutton_address);
  // set_memory();

  int content=view_word(addr);

  char buffer[100] = "";
  sprintf(buffer,"%06X",content);
  gtk_entry_set_text(GTK_ENTRY(entry_content),(const char*)&buffer);
  buffer[0]='\0';
}

void on_entry_jump_addr_activate()
{
	int addr = strtol(gtk_entry_get_text(GTK_ENTRY(entry_jump_addr)), NULL, 0);

	GtkTreeIter iter;
	gboolean valid =
		gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list_store), &iter);
	for(int i = 0; i < addr && valid; i++) {
		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(list_store), &iter);
	}

	GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(list_store),
			&iter);

	gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(treeview),
			path, NULL, TRUE, 0, 0);
}

void on_button_write_address_clicked()
{
	int addr =
	  gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinbutton_address));

	int content = strtol(gtk_entry_get_text(GTK_ENTRY(entry_content)), NULL, 0);
	// printf("%d\n", content);
	write_mem(addr, content);
	// printf("%d\n", mem[addr]);
	set_memory();
}

void on_button_write_word_clicked()
{
	int addr =
	  gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinbutton_address));

	int content = strtol(gtk_entry_get_text(GTK_ENTRY(entry_content)), NULL, 0);
	write_word(addr, content);
	set_memory();
}

void on_button_update_memory_clicked()
{
	//gtk_label_set_text(GTK_LABEL(label_reg_X_val),"hello");

	int port_no =
	  gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinbutton_io));

	const char *str = gtk_entry_get_text(GTK_ENTRY(entry_io_entry));

	enter_str_to_port(port_no, str);
	
}

void on_button_io_view_port_value_clicked()
{
	int port_no =
	  gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinbutton_io));

	char str[MAX_LEN] = "";
	read_str_from_port(str,port_no);

	gtk_entry_set_text ((GtkEntry *)entry_io_entry,
                    str);
}

int main(int argc, char *argv[])
{
    GError     *error = NULL;

    start();
   
    /* Init GTK+ */
    gtk_init( &argc, &argv );
    /* Create new GtkBuilder object */
    builder = gtk_builder_new();
    /* Load UI from file. If error occurs, report it and quit application.
     * Replace "tut.glade" with your saved project. */
    if( ! gtk_builder_add_from_file( builder, "src/window_main.glade", &error ) )
    {
        g_warning( "%s", error->message );
        g_free( error );
        return( 1 );
    }

   
    /* Get main window pointer from UI */
    window = GTK_WIDGET( gtk_builder_get_object( builder, "window_main" ) );

    /* Connect signals */
    gtk_builder_connect_signals( builder, NULL );

	GtkWidget *text_view_errors =
		GTK_WIDGET(gtk_builder_get_object(builder, "text_view_errors"));
	text_buffer_errors =
		gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view_errors));
    /*For register buttons*/
        label_reg_A_val=
                GTK_WIDGET(gtk_builder_get_object(builder, "label_reg_A_val"));
	label_reg_X_val=
                GTK_WIDGET(gtk_builder_get_object(builder, "label_reg_X_val"));
	label_reg_L_val=
                GTK_WIDGET(gtk_builder_get_object(builder, "label_reg_L_val"));
	label_reg_PC_val=
                GTK_WIDGET(gtk_builder_get_object(builder, "label_reg_PC_val"));
	label_reg_SW_val=
                GTK_WIDGET(gtk_builder_get_object(builder, "label_reg_SW_val"));

    /*For dec to hex and hex to dec conversions*/
        entry_dec=
	        GTK_WIDGET(gtk_builder_get_object(builder, "entry_dec"));
        entry_hex=
                GTK_WIDGET(gtk_builder_get_object(builder, "entry_hex"));

   /*For checking memory contents*/
        entry_content=
	        GTK_WIDGET(gtk_builder_get_object(builder, "entry_content"));
        spinbutton_address=
	        GTK_WIDGET(gtk_builder_get_object(builder, "spinbutton_address"));

   /*For checking I/O contents*/
	 entry_io_entry=
	        GTK_WIDGET(gtk_builder_get_object(builder, "entry_io_entry"));
        spinbutton_io=
	        GTK_WIDGET(gtk_builder_get_object(builder, "spinbutton_io"));


   /*For displaying memory contents in scrollable window*/
	scrolledwindow_mem =
	        GTK_WIDGET(gtk_builder_get_object(builder, "scrolledwindow_mem"));

	// add editor
	editor = gui_editor_new();
	GtkWidget *scrolledwindow_code =
		GTK_WIDGET(gtk_builder_get_object(builder, "scrolledwindow_code"));
	gtk_container_add(GTK_CONTAINER(scrolledwindow_code), editor->widget);
	text_view_code = editor->widget;
	gui_editor_show(editor);

	entry_jump_addr =
		GTK_WIDGET(gtk_builder_get_object(builder, "entry_jump_addr"));
	
	attach_memory_list();
    /* Destroy builder, since we don't need it anymore */
    g_object_unref( G_OBJECT( builder ) );

    /* Show window. All other widgets are automatically shown by GtkBuilder */
    // gtk_widget_show_all( window );

    /* Start main loop */
    gtk_main();
	// gui_editor_destroy(editor);
	// gtk_widget_destroy(window);
   
    return( 0 );
}
