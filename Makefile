CC = gcc
CFLAGS = -g -Wall -std=c99
GTK_COM_FLAGS = `pkg-config --cflags gtk+-3.0` \
				`pkg-config --cflags gtksourceview-3.0`
GTK_LINK_FLAGS = `pkg-config --libs gtk+-3.0` \
				 `pkg-config --libs gtksourceview-3.0` -export-dynamic

SRC = src
default: simulator_gui

executable = simulator

simulator_com_objects = strmap.o helper.o assembler.o loader.o archi.o\
					instructions.o runner.o simulator.o

simulator_cli_objects = cli.o

simulator_gui_objects = gui_editor.o gui.o

simulator_gui: $(simulator_com_objects) $(simulator_gui_objects)
	$(CC) $(CFLAGS) -o $(executable) $^ $(GTK_LINK_FLAGS)

simulator_cli: $(simulator_com_objects) $(simulator_cli_objects)
	$(CC) $(CFLAGS) -o $(executable) $^

gui_editor.o: $(SRC)/gui_editor.c $(SRC)/gui_editor.h

gui.o: $(SRC)/gui.c $(SRC)/gui_editor.h $(SRC)/assembler.h $(SRC)/simulator.h \
	$(SRC)/runner.h $(SRC)/loader.h

$(simulator_gui_objects):
	$(CC) $(CFLAGS) -c $< $(GTK_COM_FLAGS)

$(simulator_cli_objects):
	$(CC) $(CFLAGS) -c $<

$(simulator_com_objects):
	$(CC) $(CFLAGS) -c $<

assembler_test: strmap.o helper.o assembler.o assembler_test.o
	$(CC) $(CFLAGS) -o $@ $^

strmap.o: $(SRC)/strmap.c $(SRC)/strmap.h

helper.o: $(SRC)/helper.c $(SRC)/helper.h

assembler.o: $(SRC)/assembler.c $(SRC)/assembler.h $(SRC)/strmap.h $(SRC)/helper.h

archi.o: $(SRC)/archi.c $(SRC)/archi.h

loader.o: $(SRC)/loader.c $(SRC)/loader.h $(SRC)/archi.h

instructions.o: $(SRC)/instructions.c $(SRC)/instructions.h $(SRC)/archi.h

runner.o: $(SRC)/runner.c $(SRC)/runner.h $(SRC)/archi.h $(SRC)/instructions.h

simulator.o: $(SRC)/simulator.c $(SRC)/simulator.h $(SRC)/helper.h $(SRC)/archi.h $(SRC)/instructions.h

cli.o: $(SRC)/cli.c $(SRC)/simulator.h $(SRC)/runner.h $(SRC)/loader.h $(SRC)/assembler.h

assembler_test.o: assembler_test.c $(SRC)/assembler.h $(SRC)/helper.h
	$(CC) $(CFLAGS) -c $<

.PHONY: clean_assembler_test

.PHONY: clean

clean_assembler_test:
	rm assembler_test *.o

clean:
	rm $(executable) *.o
